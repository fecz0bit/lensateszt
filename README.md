# README #

### What is this repository for? ###

* Lensa Tesztfeladat

### How do I get set up? ###

* A projektet Laravel 5.2-ben készítettem el.
* Kivételesen nem raktam semmit gitignore-ra, hanem tisztán az egészet felpusholtam GIT-re, így ha minden igaz nem szükséges telepítés, elég a /public mappára irányítani az apache-ot.
* **Rendszer követelménye**:
* 5.6-os verziószámú php vagy újabb.
* MySql, "lensa" nevű adatbázissal.
* Db usernév és jelszó az .ENV fájlban található, jelenleg root.
* Adatbázis migrációra két folyamat van:
* **a.** Csatoltam adatbázis mentést, ez gyorsabb, de nem szép.
* **b.** Ha van fent Composer a gépen, akkor telepítéskor használható a

```
#!php artisan migrate

php artisan migrate

```
és a 

```
#!php artisan migrate:refresh --seed
php artisan migrate:refresh --seed

```
* **Figyelem: **
* Az egymillió sor migrate-je úgy 10 percet vesz igénybe php által, mivel ezeket dinamikus generálom össze. Maga a folyamat a database/seeds/MillionSeeder.php fájlban van.
* A public mappában található a js fájlom, valószínűleg angularban jobb lett volna, de azt még most tanulom és nem érzem nagyon magabiztosnak a tudásomat.
* Ha kérdés van keressetek bátran :)

### Who do I talk to? ###

* Ferenc Mile
* devferencmile@gmail.com (már minden foglalt volt, duh)
* 20/5839583