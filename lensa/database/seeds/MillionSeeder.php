<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MillionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('hu_HU');

        $limit = 1000000;
        $person = [];
        for ($i = 0; $i < $limit; $i++) {
                $rand = rand(1, 2);
                if ($rand === 1) {
                    $person['gender'] = 'male';
                    $person['sex'] = 'férfi';
                } else {
                    $person['gender'] = 'female';
                    $person['sex'] = 'nő';
                }
            DB::table('million')->insert([ //,
                'firstname' => $faker->firstName($gender = $person['gender']),
                'lastname' => $faker->lastName,
                'sex' => $person['sex'],
                'birthdate' => $faker->dateTimeThisCentury($max = 'now'),
                'talent' => $faker->randomElement($array = array ('Sport', 'Tudományok', 'Matematika')),
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }
}
