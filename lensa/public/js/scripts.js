$.ajaxSetup({
    headers: {
        'X-CSRF-Token': $('input[name="_token"]').val()
    }
});
$(document).ready(function() {
    var oTable = $('#lensa').DataTable({
        language: {
            url: '/js/Hungarian.json'
        },
        dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>" +
            "<'row'<'col-xs-12't>>" +
            "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
        processing: true,
        serverSide: true,
        ajax: {
            url: '/sex',
            type: "post",
            data: function(d) {
                d.sex = {
                    sex: $('#sex option:selected').val()
                };
                d.talent = {
                    talent: $('#talent option:selected').val()
                };
                d.birthdate = {
                    birthdate: $('#birthdate option:selected').val()
                };
            },
        },
        columns: [{
            data: 'firstname',
            name: 'firstname'
        }, {
            data: 'lastname',
            name: 'lastname'
        }, {
            data: 'sex',
            name: 'sex'
        }, {
            data: 'birthdate',
            name: 'birthdate'
        }, {
            data: 'talent',
            name: 'talent'
        }],
    });

    $('#search-form').on('submit', function(e) {
        oTable.draw();
        e.preventDefault();
    });

});
