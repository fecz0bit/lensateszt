<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\MillionModel;
use Yajra\Datatables\Facades\Datatables;
use DB;
use Carbon\Carbon;

class MillionController extends Controller
{
    public function index()
    {
        return view('app.index');
    }

    public function getSexData(Request $request)
    {
        $yearsback = Carbon::now()->subYears(25)->toDateTimeString();
        $sex = '';
        if (isset($request->get('sex')['sex'])) {
            $sex = $request->get('sex')['sex'];
        $talent = '';
        if (isset($request->get('talent')['talent'])) {
            $talent = $request->get('talent')['talent'];
        }
        }
        $birthdate = '';
        if (isset($request->get('birthdate')['birthdate'])) {
            $birthdate = $request->get('birthdate')['birthdate'];
        }
        $mil = DB::table('million')->select(['firstname', 'lastname', 'sex', 'birthdate', 'talent']);
        return Datatables::of($mil)
            ->filter(function ($query) use ($request, $sex, $talent, $birthdate, $yearsback) {
                if ($sex != '') {
                    $query->where('sex', 'like', "%{$sex}%");
                }
                if ($talent != '') {
                    $query->where('talent', 'like', "%{$talent}%");
                }
                if ($birthdate != '') {
                    $query->where('birthdate', $birthdate, $yearsback);
                }
            })
            ->make(true);
    }

}
