<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MillionModel extends Model
{
    protected $table = 'million';
    protected $fillable = [ 'id', 'created_at', 'updated_at', 'firstname', 'lastname', 'sex', 'birthdate', 'talent',];

}
