@extends('app')

@section('content')
        <div class="row">
            <div class="col-md-12">
                <form method="POST" id="search-form" class="form-inline" role="form">
                    <div class="form-group">
                        <label for="name">Nem: </label>
                        <select name="sex" id="sex" class="form-control">
                            <option value="">Összes</option>
                            <option value="Nő">Nő</option>
                            <option value="Férfi">Férfi</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Tehetség: </label>
                        <select name="talent" id="talent" class="form-control">
                            <option value="">Összes</option>
                            <option value="Sport">Sport</option>
                            <option value="Tudomány">Tudomány</option>
                            <option value="Matematika">Matematika</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">25 évnél: </label>
                        <select name="birthdate" id="birthdate" class="form-control">
                            <option value="NOT LIKE">Összes</option>
                            <option value=">">Fiatalabb</option>
                            <option value="<">Idősebb</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Szűrés</button>
                </form>
                <hr>
            </div>
        </div>
        <div class="row">
            <table id="lensa" class="table table-hover" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Vezetéknév</th>
                        <th>Keresztnév</th>
                        <th>Nem</th>
                        <th>Születési dátum</th>
                        <th>Tehetsége</th>
                    </tr>
                </thead>
            </table>
        </div>
    {{ csrf_field() }}
@endsection

@section('scripts')

@endsection

